from src import Engine

world = Engine.Engine("Pool Game", 800, 600)

# First movement
initialForce = 20
direction = [-1, 0, 1]
massOfTheBall = 2
rozamientoDinamico = 0.3
rozamientoEstatico = 0.6

def callBackFunction(caller, timer_event):
    ball.move(world.staticObjects)
    world.render()

tableAttribute = {'SetXLength': 20, 'SetYLength': 16, 'SetZLength':1}
table = world.addObjectToWorld("cube", tableAttribute)
table.setPosition([0, 0, 0])
table.setTexture("pool")
table.rotate(90, [1, 0, 0])
table.addPhysicsParameters(400, 0.8, 0.5)
world.staticObjects.remove(table)

tableSideAttribute = {'SetXLength': 2, 'SetYLength':3 , 'SetZLength':16}
tableRight = world.addObjectToWorld("cube", tableSideAttribute)
tableRight.setPosition([-11, 1, 0])
tableRight.setColor([0.54, 0.42, 0.38])

tableLeft = world.addObjectToWorld("cube", tableSideAttribute)
tableLeft.setPosition([11, 1, 0])
tableLeft.setColor([0.54, 0.42, 0.38])

tableFrontAndBackAttribute = {'SetXLength': 20, 'SetYLength':3 , 'SetZLength':2}
tableFront = world.addObjectToWorld("cube", tableFrontAndBackAttribute)
tableFront.setPosition([0, 1, 9])
tableFront.setColor([0.54, 0.42, 0.38])

tableBack = world.addObjectToWorld("cube", tableFrontAndBackAttribute)
tableBack.setPosition([0, 1, -9])
tableBack.setColor([0.54, 0.42, 0.38])

ballAttribute = {'SetRadius' : 1, 'SetThetaResolution':50, 'SetPhiResolution':50}
ball = world.addObjectToWorld("sphere", ballAttribute)
ball.setPosition([0, 1, 0])
ball.addPhysicsParameters(massOfTheBall, rozamientoEstatico, rozamientoDinamico)
ball.setTexture("bola")
world.physics.applyForce(ball, world.staticObjects, initialForce * 100, direction)

world.eventHandler("TimerEvent", callBackFunction)
world.startLoop()
