import vtk
import math
import numpy as np

class Physics:
    def __init__(self):
        self.gravity = 10
    
    def applyForce(self, worldObject, staticWorldObjects, force, direction):
        if(self.isForceBreakFriction(worldObject, force)):
            dinamicFrictionForce = self.calculateWeight(worldObject.mass) * worldObject.muDinamic
            totalForces = force - dinamicFrictionForce
            initialVelocity = (totalForces / worldObject.mass) / 1000
            worldObject.movDirection = direction
            worldObject.velocity = ([direction[0] * initialVelocity, direction[1] * initialVelocity, direction[2] * initialVelocity])
            breaking = (-dinamicFrictionForce / worldObject.mass) / 1000
            worldObject.aceleration = ([direction[0] * breaking, direction[1] * breaking, direction[2] * breaking])
            self.calculateIntersectionPoint(worldObject, staticWorldObjects, direction)
            print("Vel:", worldObject.velocity)
            print("Dir:", worldObject.movDirection, "\n")
            
    def isForceBreakFriction(self, worldObject, force):
        staticFrictionForce = self.calculateWeight(worldObject.mass) * worldObject.muStatic
        if staticFrictionForce < force:
            return True
        else:
            return False

    def calculateWeight(self, mass):
        return mass * self.gravity
    
    def calculateIntersectionPoint(self, dinamicObject, staticWorldObjects, direction):
        for worldObject in staticWorldObjects:
            XLength = worldObject.shapeActor.shapePrimitive.source.GetXLength()
            ZLength = worldObject.shapeActor.shapePrimitive.source.GetZLength()
            if(XLength < ZLength):
                if(worldObject.position[0] < 0):
                    a = [worldObject.position[0] + XLength / 2, -(ZLength / 2)]
                    b = [worldObject.position[0] + XLength / 2, +(ZLength / 2)]
                else:
                    a = [worldObject.position[0] - XLength / 2, -(ZLength / 2)]
                    b = [worldObject.position[0] - XLength / 2, +(ZLength / 2)]

            else:
                if(worldObject.position[2] < 0):
                    a = [-(XLength / 2), worldObject.position[2] + ZLength / 2]
                    b = [+(XLength / 2), worldObject.position[2] + ZLength / 2]
                else:
                    a = [-(XLength / 2), worldObject.position[2] - ZLength / 2]
                    b = [+(XLength / 2), worldObject.position[2] - ZLength / 2]
            
            x1 = a[0]
            y1 = a[1]
            x2 = b[0]
            y2 = b[1]
            
            x3 = dinamicObject.position[0]
            y3 = dinamicObject.position[2]
            x4 = dinamicObject.position[0] + direction[0]
            y4 = dinamicObject.position[2] + direction[2]

            den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
            if(den == 0):
                continue

            t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den
            u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den

            if(t >= 0 and t <= 1 and u > 0):
                xPoint = x1 + t * (x2 - x1)
                yPoint = dinamicObject.position[1]
                zPoint = y1 + t * (y2 - y1)
                intersectedPoint = [xPoint, yPoint, zPoint]
                dinamicObject.collisionPoint = intersectedPoint
                dinamicObject.collisionObject = worldObject
                
            else:
                continue
        return [0, 0, 0]

    
