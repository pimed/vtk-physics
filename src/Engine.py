import vtk

from src import WorldObject
from src import Physics

class Engine:
    def __init__(self, windowName, width, height):
        # Create a renderer
        self.renderer = vtk.vtkRenderer()
        self.renderer.SetBackground(0.60, 0.65, 0.92)       
        
        # Create a camera
        self.camera = vtk.vtkCamera()
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition(0, 70, 20)

        # Create a window
        self.window = vtk.vtkRenderWindow()
        self.window.SetWindowName(windowName)
        self.window.SetSize(width, height)
        self.window.AddRenderer(self.renderer)

        # Physics
        self.physics = Physics.Physics()
        self.dinamicObjects = []
        self.staticObjects = []

        # Create a interactor
        self.interactor = vtk.vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(self.window)
        self.interactor.Initialize()

    def startLoop(self):
        self.renderer.SetActiveCamera(self.camera)
        self.window.Render()
        self.interactor.Start()
    
    def render(self):
        self.window.Render()

    def eventHandler(self, event, function):
        self.interactor.CreateRepeatingTimer(30)
        self.interactor.AddObserver(event, function)

    def renderObjectToWorld(self, worldObject):
        self.renderer.AddActor(worldObject)

    def addObjectToWorld(self, shape, attributes):
        worldObject = WorldObject.WorldObject(shape, attributes)
        if(shape == "sphere"):
            self.dinamicObjects.append(worldObject)
        elif(shape == "cube"):
            self.staticObjects.append(worldObject)
        self.renderObjectToWorld(worldObject.shapeActor.actor)
        return worldObject
    





