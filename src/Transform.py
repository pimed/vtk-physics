import vtk

class Transform:
    def __init__(self):
        self.transform = vtk.vtkTransform()
        self.transformFilter = vtk.vtkTransformPolyDataFilter()

    def rotateObject(self, source, angle, direction):
        self.transform.RotateWXYZ(angle, direction[0], direction[1], direction[2])
        self.transformFilter.SetTransform(self.transform)
        self.transformFilter.SetInputConnection(source.GetOutputPort())
        self.transformFilter.Update()