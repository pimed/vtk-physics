from src import ShapeFactory

class Source:
    def __init__(self, shape, attributes):
        self.shapeFactory = ShapeFactory.ShapeFactory()
        self.source = self.shapeFactory.createNewShape(shape)
        self.shape = shape
        self.addAttributes(attributes)
    
    def addAttributes(self, attributes):
        for attribute in attributes:
            getattr(self.source, attribute)(attributes[attribute])
        self.source.Update()

    