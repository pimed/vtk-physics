import vtk

class Mapper:
    def __init__(self):
        self.mapper = vtk.vtkPolyDataMapper()
        self.mapperShape = None

    def mapShapePrimitive(self, source):
        self.mapper.SetInputData(source.GetOutput())
    
    def mapTextureToShape(self, source, shape):
        if shape == "cube":    
            self.mapperShape = vtk.vtkTextureMapToPlane()
        elif shape == "sphere":
            self.mapperShape = vtk.vtkTextureMapToSphere()

        self.mapperShape.SetInputConnection(source.GetOutputPort())
        self.mapper.SetInputConnection(self.mapperShape.GetOutputPort())
    
    def mapTransformToSource(self, transformFilter):
        self.mapper.SetInputConnection(transformFilter.GetOutputPort())
