import vtk

class ShapeFactory:
    def __init__(self):
        self.shapes = {
            "cone" : "vtkConeSource",
            "cube" : "vtkCubeSource",
            "sphere": "vtkSphereSource",
            "cylinder": "vtkCylinderSource",
            "plane": "vtkPlaneSource"
        } 

    def createNewShape(self, shape):
        vtkPrimitive = self.shapes.get(shape)
        shapePrimitive = getattr(vtk, vtkPrimitive)()
        return shapePrimitive

