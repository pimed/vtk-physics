import vtk

from src import Source
from src import Mapper
from src import Transform
from src import TextureFactory

class Actor:
    def __init__(self, shape, attributes):
        self.shapePrimitive = Source.Source(shape, attributes)
        self.transform = Transform.Transform()
        self.textureFactory = TextureFactory.TextureFactory()
        self.shapeMapper = Mapper.Mapper()
        self.shapeMapper.mapShapePrimitive(self.shapePrimitive.source)
        self.actor = vtk.vtkActor()
        self.linkMapperToActor(self.shapeMapper)
    
    def linkMapperToActor(self, generalMapper):
        self.actor.SetMapper(generalMapper.mapper)

    # Characteristics
    def setPosition(self, coordinates):
        self.actor.SetPosition(coordinates[0], coordinates[1], coordinates[2]) 

    def setColor(self, rgb):
        self.actor.GetProperty().SetColor(rgb[0], rgb[1], rgb[2])
    
    def setTexture(self, textureName):
        textureMapper = Mapper.Mapper()
        textureMapper.mapTextureToShape(self.shapePrimitive.source, self.shapePrimitive.shape)
        self.linkMapperToActor(textureMapper)
        texture = self.textureFactory.createNewTexture(textureName)
        self.actor.SetTexture(texture)
    
    # Movements
    def rotate(self, angle, direction):
        self.actor.RotateWXYZ(angle, direction[0], direction[1], direction[2])

        

    