import vtk
import math
from src import Physics
from src import Actor


class WorldObject:
    def __init__(self, shape, attributes):
        self.shapeActor = Actor.Actor(shape, attributes)
        self.position = [0, 0, 0]
        self.velocity = [0, 0, 0]
        self.aceleration = [0, 0, 0]
        self.mass = None
        self.muStatic = None
        self.muDinamic = None
        self.collisionPoint = None
        self.collisionObject = None
        self.movDirection = None
        self.physics = Physics.Physics()
    
    # Characteristics
    def addPhysicsParameters(self, mass, muStatic, muDinamic):
        self.mass = mass
        self.muStatic = muStatic
        self.muDinamic = muDinamic

    def setPosition(self, position):
        self.position[0] = position[0]
        self.position[1] = position[1]
        self.position[2] = position[2]
        self.shapeActor.setPosition(position)

    def setColor(self, rgb):
        self.shapeActor.setColor(rgb)

    def setTexture(self, textureName):
        self.shapeActor.setTexture(textureName)

    # Movement
    def rotate(self, angle, direction):
        self.shapeActor.rotate(angle, direction)

    def move(self, worldObjects):
        self.position[0] = self.position[0] + self.velocity[0]
        self.position[1] = self.position[1] + self.velocity[1]
        self.position[2] = self.position[2] + self.velocity[2]
        
        self.shapeActor.setPosition(self.position)

        rotateVelocity = (abs(self.velocity[0]) + abs(self.velocity[2]) / 2) * 100
        self.shapeActor.rotate(rotateVelocity, [self.velocity[2], self.velocity[1], -self.velocity[0]])
        
        self.velocity[0] = self.velocity[0] + self.aceleration[0]
        self.velocity[1] = self.velocity[1] + self.aceleration[1]
        self.velocity[2] = self.velocity[2] + self.aceleration[2]

        if( abs(self.velocity[0]) <= 0.01):
            self.velocity[0] = 0
            self.aceleration[0] = 0
        if( abs(self.velocity[1]) <= 0.01):
            self.velocity[1] = 0
            self.aceleration[1] = 0
        if( abs(self.velocity[2]) <= 0.01):
            self.velocity[2] = 0
            self.aceleration[2] = 0
        
        self.isCollisionedStatic(worldObjects, self.movDirection)

    def isCollisionedStatic(self, staticWorldObjects, direction):
        radius = self.shapeActor.shapePrimitive.source.GetRadius()
        xSeparation = abs(self.collisionPoint[0] - (self.position[0] + (radius * direction[0])))
        zSeparation = abs(self.collisionPoint[2] - (self.position[2] + (radius * direction[2])))
        if(xSeparation < 0.5 and zSeparation < 0.5):  
            print(self.collisionObject.position)
            if self.collisionObject.position[0] > 0:
                self.velocity[0] = -self.velocity[0]
                self.movDirection[0] = -self.movDirection[0]
                self.aceleration[0] = -self.aceleration[0]
            elif self.collisionObject.position[0] < 0:
                self.velocity[0] = -self.velocity[0]
                self.movDirection[0] = -self.movDirection[0]
                self.aceleration[0] = -self.aceleration[0]
            elif self.collisionObject.position[2] > 0:
                self.velocity[2] = -self.velocity[2]
                self.movDirection[2] = -self.movDirection[2]
                self.aceleration[2] = -self.aceleration[2]
            elif self.collisionObject.position[2] < 0:
                self.velocity[2] = -self.velocity[2]
                self.movDirection[2] = -self.movDirection[2]
                self.aceleration[2] = -self.aceleration[2]

            self.physics.calculateIntersectionPoint(self, staticWorldObjects, self.movDirection)
            print("Col: ", self.collisionObject.position)
            print("Vel: ", self.velocity)
            print("Dir: ", self.movDirection, "\n")
        
    
