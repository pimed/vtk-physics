import vtk 

class TextureFactory:
    def __init__(self):
        self.reader = vtk.vtkJPEGReader()
        self.textures = {
            "wood" : "resources/wood.jpg",
            "rock" : "resources/rock.jpg",
            "pool" : "resources/pool.jpg",
            "bola" : "resources/bola15.jpg"
        }
    
    def createNewTexture(self, textureName):
        self.reader.SetFileName(self.textures.get(textureName))
        texture = vtk.vtkTexture()
        texture.SetInputConnection(self.reader.GetOutputPort())
        # You can add characteristic to the texture: texture.RepeatOff()
        texture.InterpolateOn()
        return texture
